---
layout: post
title:  "Blog Redesign"
subtitle: "Migrating from GitHub to GitLab"
date:   2018-06-05
background: '/img/posts/06.jpg'
---

Following the acquisition of GitHub by Microsoft, I decided to migrate all my repositories from GitHub to GitLab.  The process was rather painless and streamlined.  As for the GitHub Pages blog, I copied that over to GitLab as well, but I took this migration as an opportunity to redesign the blog.  After a quick search for blog themes I downloaded the *Clean Blog* template from [Start Bootstrap](https://startbootstrap.com/).  This is a nice modern looking theme that works pretty well on any device.  Blog theme settled!

As for the old blog, the source is available on my [GitLab](https://gitlab.com/kobayash1).  I probably won't bother migrating old posts or rehosting it elsewhere.
