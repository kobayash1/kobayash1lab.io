---
layout: post
title:  "Saturday Climbing Session 🧗"
subtitle: "Asian American Chicago Network"
date:   2022-07-23
background: '/img/posts/fa-block37.jpg'
---

Let's go bouldering!  Beginners welcome!  We'll get food at Urbanspace nearby after climbing.

## Where
[First Ascent @ Block 37](https://faclimbing.com/chicago/block-37/)  
108 N State St  
Suite 420  
Chicago IL, 60602
## When
3:00 PM Saturday August 6
## Rates
One day pass is $23.  Shoe rental is $6.  
More information on 10-day passes and memberships [here](https://faclimbing.com/chicago/first-visit/day-pass-visit/).
## Logistics
We'll meet up at 3 PM at the front desk to sign waivers, watch the safety video, and fit rental shoes.  The gym suggests wearing comfortable athletic clothing that allows you to move freely.  Pants or long shorts that cover your knees are best.  If you will be renting climbing shoes, they also recommend wearing socks. 
## Getting There
#### By Blue & Red Line Trains
Take the Blue Line to Washington or the Red Line to Lake and follow signs to the CTA Pedway that connects the Blue and Red Lines. You can enter Block 37 via the Pedway – without having to exit the building!
#### By Bus
Take the 56 Bus from the west and exit at Washington & State. Take the 29 Bus from the north or south and exit at State & Randolph.

[<img src="/img/posts/fa-block37.jpg" width="350"/>](/img/posts/fa-block37.jpg)
