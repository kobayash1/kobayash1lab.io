---
layout: page
title: About Me
description: This is what I do.
background: '/img/bg-about.jpg'
---

Sup.  I'm Chris and this is my blog.  I will sporadically post thoughts and musings here related to my interests, hobbies, and studies.  Said interests, hobbies, and studies include but are not limited to the following:

* Linux
* k-pop
* Tekken and other fighting games
* sports
* cars
* nursing

Enjoy your stay!
