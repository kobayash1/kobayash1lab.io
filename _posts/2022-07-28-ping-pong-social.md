---
layout: post
title:  "Ping Pong Social 🏓"
subtitle: "Asian American Chicago Network"
date:   2022-07-28
background: '/img/posts/ping-pong-spin.jpg'
---

Let's meetup for cocktails and table tennis in River North! 🥂🏓  
Bring your friends and RSVP!  We'll book reservations for ping pong tables based on RSVP numbers.

## Where
[SPIN Chicago](https://wearespin.com/gallery/chicago-venue/)  
344 N State St  
Chicago, IL 60654
## When
9:00 PM Saturday August 20
## Getting There
#### By Train
Red Line to Grand/State, then walk 4 minutes south on State St.  
Brown Line to Merchandise Mart, then walk 8 minutes east on Kinzie St.  
Blue Line to Washington, then walk 8 minutes north on State St.  
Orange Line to State/Lake, then walk 4 minutes north on State St.  

[<img src="/img/posts/ping-pong-spin.jpg" width="350"/>](/img/posts/ping-pong-spin.jpg)
