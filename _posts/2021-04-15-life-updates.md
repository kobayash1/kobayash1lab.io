---
layout: post
title:  "Life Updates"
subtitle: "Some career progressions"
date:   2021-04-15
background: '/img/posts/nursing-banner.png'
---

Right.  Some updates since my last post in 2018.  I achieved my Associate's degree in nursing last December.  Passed the NCLEX in January.  Accepted my first RN position at the end of January.  Started the new job in March.  Finishing up orientation, just a couple weeks left until they send me off on my own.  Also, I obtained my ACLS certification last week.  Looking to start my RN to BSN program in either May or June.  Leaning towards May.

Details!  I'll be doing nights on a telemetry unit at a hospital in Chicago.  I should probably move closer.  I also added my LinkedIn to the bottom of the website.  Network with me!  👍

Ciao for now.
